/* Symphony Environment —> z. B. Symphony.Context.get('env').page; */

jQuery(window).load(function(){
  
  // Save via Cmd-S
  jQuery(document).keydown(function(event) {
    // If Control or Command key is pressed and the S key is pressed run save function. 83 is the key code for S. https://stackoverflow.com/questions/21658829/jquery-capture-ctrl-s-and-command-s-mac
    if((event.ctrlKey || event.metaKey) && event.which == 83) {
      event.preventDefault();
      jQuery('input[name="action[save]"]').trigger('click');
      return false;
    };
  });

  
});