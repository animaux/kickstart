<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	extension-element-prefixes="dyn exslt"
  xmlns:exslt="http://exslt.org/common"
  xmlns:dyn="http://exslt.org/dynamic">

<xsl:import href="../utilities/format-date.xsl"/>

<xsl:template name="zeitraum">
	<xsl:param name="start" select="date/date/start"/>
	<xsl:param name="ende" select="date/date/end"/>
	<xsl:param name="startzeit" select="date/date/start/@time"/>
	<xsl:param name="endzeit" select="date/date/end/@time"/>
	<xsl:param name="showtime" select="'no'"/>
	
	<xsl:variable name="start-jahr">
	  <xsl:value-of select="substring($start, 1, 4)"/>
	</xsl:variable>
	<xsl:variable name="start-monat">
	  <xsl:value-of select="substring($start, 6, 2)"/>
	</xsl:variable>
	<xsl:variable name="start-tag">
	  <xsl:value-of select="substring($start, 9, 2)"/>
	</xsl:variable>
	<xsl:variable name="ende-jahr">
	  <xsl:value-of select="substring($ende, 1, 4)"/>
	</xsl:variable>
	<xsl:variable name="ende-monat">
	  <xsl:value-of select="substring($ende, 6, 2)"/>
	</xsl:variable>
	<xsl:variable name="ende-tag">
	  <xsl:value-of select="substring($ende, 9, 2)"/>
	</xsl:variable>
	
	<!-- Debug -->
  <!-- <xsl:text>###</xsl:text><br />
  <xsl:text>Start: </xsl:text><xsl:value-of select="$start"/><br />
  <xsl:text>Ende: </xsl:text><xsl:value-of select="$ende"/><br />
  <xsl:text>Start-Jahr: </xsl:text><xsl:value-of select="$start-jahr"/><br />
  <xsl:text>Start-Monat: </xsl:text><xsl:value-of select="$start-monat"/><br />
  <xsl:text>Start-Tag: </xsl:text><xsl:value-of select="$start-tag"/><br />
  <xsl:text>Ende-Jahr: </xsl:text><xsl:value-of select="$ende-jahr"/><br />
  <xsl:text>Ende-Monat: </xsl:text><xsl:value-of select="$ende-monat"/><br />
  <xsl:text>Ende-Tag: </xsl:text><xsl:value-of select="$ende-tag"/><br />
  <xsl:text>###</xsl:text><br /> -->
	
	<xsl:choose>
	  
	  <!-- Identischer Tag oder kein Zeitraum -->
		<xsl:when test="(substring($ende, 1) = '') or ($start = $ende)">
      <xsl:call-template name="format-date">
        <xsl:with-param name="date" select="$start" />
        <xsl:with-param name="format" select="'d. M Y'" />
      </xsl:call-template>
      <xsl:if test="$showtime='Yes'">
        <xsl:text>, </xsl:text>
        <xsl:choose>
          <!-- nur Startzeit oder identische Zeit -->
        	<xsl:when test="(substring($endzeit, 1) = '') or ($startzeit = $endzeit)">
            <xsl:value-of select="$startzeit"/>
          </xsl:when>
        	<xsl:otherwise>
            <xsl:value-of select="$startzeit"/>
            <xsl:text>–</xsl:text>
            <xsl:value-of select="$endzeit"/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="$lang='de'">
          <xsl:text> Uhr</xsl:text>
        </xsl:if>
      </xsl:if>
	  </xsl:when>
	  
	  <!-- Identischer Monat (ohne Zeit) -->
		<xsl:when test="($start-jahr = $ende-jahr) and 
                    ($start-monat = $ende-monat) and 
                    ($showtime='No')">
	    <xsl:call-template name="format-date">
        <xsl:with-param name="date" select="$start" />
        <xsl:with-param name="format" select="'d.'" />
      </xsl:call-template>
      <xsl:text> – </xsl:text>
      <xsl:call-template name="format-date">
        <xsl:with-param name="date" select="$ende" />
        <xsl:with-param name="format" select="'d. M Y'" />
      </xsl:call-template>
	  </xsl:when>
	  
	  <!-- Identisches Jahr (ohne Zeit) -->
		<xsl:when test="($start-jahr = $ende-jahr) and 
                    ($showtime='No')">
      <xsl:call-template name="format-date">
        <xsl:with-param name="date" select="$start" />
        <xsl:with-param name="format" select="'d. M'" />
      </xsl:call-template>
      <xsl:text> – </xsl:text>
      <xsl:call-template name="format-date">
        <xsl:with-param name="date" select="$ende" />
        <xsl:with-param name="format" select="'d. M Y'" />
      </xsl:call-template>
    </xsl:when>
	  
	  <!-- Alles anders -->
		<xsl:otherwise>
	    <xsl:call-template name="format-date">
        <xsl:with-param name="date" select="$start" />
        <xsl:with-param name="format" select="'d. M Y'" />
      </xsl:call-template>
      <xsl:if test="$showtime='Yes'">
        <xsl:text>, </xsl:text>
				<xsl:value-of select="$startzeit"/>
        <xsl:if test="$lang='de'">
          <xsl:text> Uhr</xsl:text>
        </xsl:if>
      </xsl:if>
      <xsl:text> – </xsl:text>
      <xsl:call-template name="format-date">
        <xsl:with-param name="date" select="$ende" />
        <xsl:with-param name="format" select="'d. M Y'" />
      </xsl:call-template>
      <xsl:if test="$showtime='Yes'">
        <xsl:text>, </xsl:text>
				<xsl:value-of select="$endzeit"/>
        <xsl:if test="$lang='de'">
          <xsl:text> Uhr</xsl:text>
        </xsl:if>
      </xsl:if>
	  </xsl:otherwise>
	</xsl:choose>
	
	
	
	


	
	
	
</xsl:template>

</xsl:stylesheet>
