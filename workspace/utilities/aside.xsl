<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:a="http://getsymphony.com/functions"
	xmlns:func="http://exslt.org/functions"
	xmlns:exslt="http://exslt.org/common"
	xmlns:dyn="http://exslt.org/dynamic"
	extension-element-prefixes="func dyn exslt a">

<xsl:import href="../utilities/substring-after-last.xsl"/>


<!-- Config -->
<xsl:variable name="galerie-max">4</xsl:variable>


<xsl:template name="aside">
  <aside>
		<xsl:for-each select="marginalien/item">
			<div class="text">
				<!-- Text -->
				<xsl:call-template name="text">
					<xsl:with-param name="level" select="'2'" />
				</xsl:call-template>
			</div>
		</xsl:for-each>
    <xsl:if test="bilder/item">
      <ul class="galerie">
        <xsl:for-each select="bilder/item">
          <li>
            <xsl:if test="count(../../bilder/item) &gt; $galerie-max and
                          position() = $galerie-max">
							<xsl:attribute name="class">plus</xsl:attribute>
						</xsl:if>
						<xsl:if test="position() &gt; $galerie-max">
							<xsl:attribute name="class">hidden</xsl:attribute>
						</xsl:if>
            <a href="{$root}/image/gross{datei/@path}/{datei/filename}" rel="gruppe{../../@id}" title="{dyn:evaluate(concat('titel-', $lang))}">
              <img src="{$root}/image/galerie{datei/@path}/{datei/filename}" alt="{dyn:evaluate(concat('titel-', $lang))}" />
            </a>
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>
    <xsl:if test="downloads/item">
      <h4>Download<xsl:if test="count(downloads/item) &gt; 1">s</xsl:if></h4>
      <ul class="downloads">
        <xsl:for-each select="downloads/item">
          <li>
            <a href="{$workspace}{datei/@path}/{datei/filename}" target="_blank">
              <span class="dok">
                <xsl:call-template name="substring-after-last">
                  <xsl:with-param name="string" select="datei/filename" />
                  <xsl:with-param name="delimiter" select="'.'" />
                </xsl:call-template>
              </span>
              <xsl:value-of select="dyn:evaluate(concat('titel-', $lang))"/>
              <xsl:text> (</xsl:text>
              <xsl:value-of select="datei/@size"/>
              <xsl:text>)</xsl:text>
            </a>
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>
  </aside>
</xsl:template>




<!-- Galerie JS -->
<xsl:template name="galerie-js">
  <script src="{$workspace}/js/jquery-3.4.1.min.js"></script>
  <script>
    $(document).ready(function() {
      $("ul.galerie li.hidden").hide();
      var mehr = $("ul.galerie li").length - <xsl:value-of select="$galerie-max" disable-output-escaping="yes"/>;
      $("ul.galerie li.plus a").append('<span>+' + mehr + '</span>');
    });
  </script>
  <xsl:call-template name="flickbox"/>
</xsl:template>




<!--  Galerien FlickityBox -->
<xsl:template name="flickbox">
	<script src="{$workspace}/js/flickity.pkgd.min.js"></script>
  <script>
    $(function(){
    
      $('ul.galerie li a').click(function(event) {
        event.preventDefault();
        
        // Daten holen
        var BilderA = $(this).parent().parent().find('a');
        var Posi = $(this).parent().index();
        
        // console.log(BilderA);
        
        // Box bauen
        var fBox = '<div class="fbox"><button class="zu">× <xsl:value-of select="a:lang('close')"/></button><ul>';
        $(BilderA).each(function() {
          var Title = $(this).attr("title");
          var Href = $(this).attr("href");
          fBox += '<li><figure><img src="'+Href+'" alt="' + Title + '"/><figcaption>' + Title + '</figcaption></figure></li>';
        });
        fBox += '</ul></div>';
        
        // Galerie anheften
				$('body').append($(fBox));
				// Flickity nur wenn mehr als 1
				if (BilderA.length &gt; 1) {
					$('.fbox ul').flickity({
						cellSelector: 'li',
						imagesLoaded: true,
						prevNextButtons: false
					}).flickity('select', Posi).focus();
				}
        
        // Galerie schließen
        $('.fbox, .zu').click(function(){
          $('.fbox').fadeOut(300, function() {
            $(this).remove();
          });
        }).children('ul').click(function(e) {
          return false;
        });
        $(document).keyup(function(e){
          if(e.keyCode === 27)
            $('.fbox').fadeOut(300, function() {
              $(this).remove();
            });
        });
                
      });
      
    });
  </script>
</xsl:template>



</xsl:stylesheet>
