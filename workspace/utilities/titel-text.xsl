<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:a="http://getsymphony.com/functions"
	xmlns:func="http://exslt.org/functions"
	xmlns:exslt="http://exslt.org/common"
	xmlns:dyn="http://exslt.org/dynamic"
	extension-element-prefixes="func dyn exslt a">

<xsl:import href="../utilities/ninja.xsl"/>

  <!-- Sprach-Logik für Text --> 
  <xsl:template name="text">
    <xsl:param name="level" select="2"/>
    <xsl:param name="textnode" select="'text'"/>
    <xsl:param name="apology" select="'Yes'"/>
    <!-- Text Sprachcheck und Ausgabe -->
    <xsl:variable name="text">
      <xsl:choose>
        <!-- HTML-Text ist in unserer Sprache vorhanden: -->
        <xsl:when test="dyn:evaluate(concat($textnode, '-', $lang, '/*'))">
          <xsl:apply-templates select="dyn:evaluate(concat($textnode, '-', $lang, '/*'))" mode="ninja">
            <xsl:with-param name="level" select="$level" />
          </xsl:apply-templates>
        </xsl:when>
        <!-- Plaintext ist in unserer Sprache vorhanden: -->
        <xsl:when test="dyn:evaluate(concat($textnode, '-', $lang, '/text()'))">
          <xsl:apply-templates select="dyn:evaluate(concat($textnode, '-', $lang, '/text()'))" mode="ninja">
            <xsl:with-param name="level" select="$level" />
          </xsl:apply-templates>
        </xsl:when>
        <!-- sonst »Deutsch« und evtl. Entschuldigung -->
        <xsl:otherwise>
          <xsl:if test="$apology='Yes'">
						<p class="notiz"><xsl:value-of select="a:lang('notext')"/></p>
					</xsl:if>
					<div lang="de">
						<xsl:apply-templates select="dyn:evaluate(concat($textnode, '-de/*')) | dyn:evaluate(concat($textnode, '-de/text()'))" mode="ninja">
							<xsl:with-param name="level" select="$level" />
						</xsl:apply-templates>
          </div>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:copy-of select="exslt:node-set($text)"/>
  </xsl:template>


  <!-- Sprach-Logik für NUR Titel --> 
  <xsl:template name="titel">
    <!-- Titel Sprachcheck und Ausgabe -->
    <xsl:variable name="titel">
      <xsl:choose>
        <xsl:when test="dyn:evaluate(concat('titel-', $lang, '/text()'))">
          <xsl:value-of select="dyn:evaluate(concat('titel-', $lang))"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="titel-de"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <!-- <xsl:value-of select="$titel"/> -->
    <xsl:call-template name="typo">
			<xsl:with-param name="text" select="$titel"/>
		</xsl:call-template>
  </xsl:template>
  
  
  <!-- Sprach-Logik für beliebigen REINEN Text --> 
  <xsl:template name="plaintext">
    <xsl:param name="textnode"/>
    <!-- Titel Sprachcheck und Ausgabe -->
		<xsl:choose>
			<xsl:when test="dyn:evaluate(concat($textnode, '-', $lang))">
				<xsl:value-of select="dyn:evaluate(concat($textnode, '-', $lang))"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="dyn:evaluate(concat($textnode, '-de'))"/>
			</xsl:otherwise>
		</xsl:choose>
  </xsl:template>

</xsl:stylesheet>
