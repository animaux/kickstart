<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:a="http://getsymphony.com/functions"
	xmlns:func="http://exslt.org/functions"
	xmlns:exslt="http://exslt.org/common"
	xmlns:dyn="http://exslt.org/dynamic"
	extension-element-prefixes="func dyn exslt a">

<!-- ### Strings ### -->
<xsl:variable name="langs">

<!-- Sprachentschuldigungen -->
<notext>
	<de>(kein Text)</de>
	<en>(Apologies. This content is only available in German language.)</en>
</notext>

<!-- Sprachen -->
<de>
	<de>Deutsch</de>
	<en>German</en>
	<ru>Немецкий</ru>
	<tr>Almanca</tr>
	<ar>ألماني</ar>
</de>
<en>
	<de>Englisch</de>
	<en>English</en>
	<ru>Английский</ru>
	<tr>İngilizce</tr>
	<ar>الإنجليزية</ar>
</en>
<ru>
	<de>Russisch</de>
	<en>Russian</en>
	<ru>Русский</ru>
	<tr>Rusça</tr>
	<ar>الروسية</ar>
</ru>
<tr>
	<de>Türkisch</de>
	<en>Turkish</en>
	<ru>Турецкий</ru>
	<tr>Türk</tr>
	<ar>اللغة التركية</ar>
</tr>
<ar>
	<de>Arabisch</de>
	<en>Arabian</en>
	<ru>Арабский</ru>
	<tr>Arapça</tr>
	<ar>اللغة العربية</ar>
</ar>




</xsl:variable>


</xsl:stylesheet>
