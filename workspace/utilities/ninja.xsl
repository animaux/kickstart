<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	extension-element-prefixes="dyn exslt my"
  xmlns:exslt="http://exslt.org/common"
  xmlns:dyn="http://exslt.org/dynamic"
  xmlns:my="my:my">

<!--
	XSLT NINJA TRANSFORMATIONS
	
	- Author: Nils Hörrmann <http://nilshoerrmann.de> based on a technique by Allen Chang <http://chaoticpattern.com> with changes and additions by Alexander Rutz <https://animaux.de>
	- See: <http://symphony-cms.com/learn/articles/view/html-ninja-technique/>
	- Version: 2.2
	- Release date: 2023-06-05

	# Example usage
	
		<xsl:apply-templates select="/data/articles/entry/body/p" mode="ninja" />
	
	# Options
	
	- level             A number to adjust the headline hierarchy, defaults to 0
	- prefix            A string or node added as first child to the matched elements
	- suffix            A string or node added as last child to the matched elements
	
	# Change log
	
	## Version 2.2
	
	- use slightly different replacement pattern from https://stackoverflow.com/a/45109160/1520177
	
	## Version 2.1
	
	- Added external links —> noreferrer
	
	## Version 2
	
	- Integrated Typo Korrekturen
	- Added external links —> _blank 
	
	## Version 1.2
	
	- Added $prefix and $suffix parameters
	
	## Version 1.1
	
	- Added missing `ninja` mode for headlines
	
	## Version 1.0
	
	- Initial release
-->

<xsl:template match="//*" mode="ninja">
	<xsl:param name="level" select="0" />
	<xsl:param name="class" />
	<xsl:param name="prefix" />
	<xsl:param name="suffix" />
	
	<!-- Create element -->
	<xsl:element name="{name()}">
	
		<!-- Class -->
		<xsl:if test="$class != ''">
			<xsl:attribute name="class">
				<xsl:value-of select="$class" />
			</xsl:attribute>
		</xsl:if>
		
		<!-- Prefix -->
		<xsl:if test="position() = 1">
			<xsl:copy-of select="$prefix" />
		</xsl:if>
	
		<!-- Apply templates for inline elements, attributes and text nodes -->
		<xsl:apply-templates select="* | @* | text()" mode="ninja">
			<xsl:with-param name="level" select="$level" />
		</xsl:apply-templates>
		
		<!-- Suffix -->
		<xsl:if test="position() = last()">
			<xsl:copy-of select="$suffix" />
		</xsl:if>
	</xsl:element>
</xsl:template>

<!--
	Attributes
-->
<xsl:template match="//@*" mode="ninja">
	<xsl:attribute name="{name(.)}">
		<xsl:value-of select="." />
	</xsl:attribute>
</xsl:template>

<!--
	Headlines
-->
<xsl:template match="h1 | h2 | h3 | h4" mode="ninja" priority="1">
	<xsl:param name="level" select="0" />
	
	<!-- Change hierarchy -->
	<xsl:element name="h{substring-after(name(), 'h') + $level}">
		<xsl:apply-templates select="* | @* | text()" mode="ninja" />
	</xsl:element>
</xsl:template>

<xsl:template match="text()" mode="ninja" priority="1">
	<xsl:call-template name="typo">
		<xsl:with-param name="text" select="."/>
	</xsl:call-template>
</xsl:template>


<!-- 
  Externe Links 
-->
<xsl:template match="a" mode="ninja" priority="1">
	<a>
		<xsl:if test="not(@target) and starts-with(@href, 'http') and not(contains(@href, /data/params/http-host))">
			<xsl:attribute name="target">_blank</xsl:attribute>
			<xsl:attribute name="rel">noreferrer</xsl:attribute>
		</xsl:if>

		<xsl:apply-templates select="* | @* | text()" mode="ninja" />
	</a>
</xsl:template>

<!-- 
  TYPO Korrekturen: 
-->

<!-- Multireplace Patterns -->
<my:params xml:space="preserve">
  <pattern><old> - </old><new> – </new></pattern>
  <pattern><old> – </old><new> – </new></pattern>
  <pattern><old> — </old><new> – </new></pattern>
  <pattern><old> , </old><new>, </new></pattern>
  <pattern><old> x </old><new> × </new></pattern>
  <pattern><old>...</old><new>…</new></pattern>
  <pattern><old>  </old><new> </new></pattern>
  <pattern><old>0-</old><new>0–</new></pattern>
  <pattern><old>1-</old><new>1–</new></pattern>
  <pattern><old>2-</old><new>2–</new></pattern>
  <pattern><old>3-</old><new>3–</new></pattern>
  <pattern><old>4-</old><new>4–</new></pattern>
  <pattern><old>5-</old><new>5–</new></pattern>
  <pattern><old>6-</old><new>6–</new></pattern>
  <pattern><old>7-</old><new>7–</new></pattern>
  <pattern><old>8-</old><new>8–</new></pattern>
  <pattern><old>9-</old><new>9–</new></pattern>
  <!-- <pattern><old>/</old><new>/​</new></pattern> -->
</my:params>


<!--
		https://stackoverflow.com/a/45109160/1520177
-->
<xsl:variable name="vrtfPats">
 <xsl:for-each select="document('')/*/my:params/*">
  <xsl:sort select="string-length(old)"
       data-type="number" order="descending"/>
   <xsl:copy-of select="."/>
 </xsl:for-each>
</xsl:variable>
<xsl:variable name="vPats" select="exslt:node-set($vrtfPats)/*"/>

<!-- This template matches all text() nodes, and calls itself recursively to performs the actual replacements. -->
<xsl:template name="string-replace-all">
		<xsl:param name="text"/>
		<xsl:param name="pos" select="1"/>
		<xsl:variable name="replace" select="$vPats[$pos]/old"/>
		<xsl:variable name="by" select="$vPats[$pos]/new"/>
		<xsl:choose>

				<!-- Ignore empty strings -->
				<xsl:when test="string-length(translate(normalize-space($text), ' ', '')) = 0"> 
						<xsl:value-of select="$text"/>
				</xsl:when>

				<!-- Return the unchanged text if the replacement is larger than the input (so no match possible) -->
				<xsl:when test="string-length($replace) > string-length($text)">
						<xsl:value-of select="$text"/>
				</xsl:when>

				<!-- If the current text contains the next pattern -->
				<xsl:when test="contains($text, $replace)">
						<!-- Perform a recursive call, each time replacing the next occurrence of the current pattern -->
						<xsl:call-template name="string-replace-all">
								<xsl:with-param name="text" select="concat(substring-before($text,$replace),$by,substring-after($text,$replace))"/>
								<xsl:with-param name="pos" select="$pos"/>
						</xsl:call-template>
				</xsl:when>

				<!-- No (more) matches found -->
				<xsl:otherwise>
						<!-- Bump the counter to pick up the next pattern we want to search for -->
						<xsl:variable name="next" select="$pos+1"/>
						<xsl:choose>
								<!-- If we haven't finished yet, perform a recursive call to process the next pattern in the list. -->
								<xsl:when test="boolean($vPats[$next])">
										<xsl:call-template name="string-replace-all">
												<xsl:with-param name="text" select="$text"/>
												<xsl:with-param name="pos" select="$next"/>
										</xsl:call-template>
								</xsl:when>

								<!-- No more patterns, we're done. Return the fully processed text. -->
								<xsl:otherwise>
										<xsl:value-of select="$text"/>
								</xsl:otherwise>
						</xsl:choose>
				</xsl:otherwise>
		</xsl:choose>
</xsl:template>








<!-- Kerntemplate -->
<xsl:template name="typo">
  <xsl:param name="text" select="$text"/>
  
  <!-- Suchen & Ersetzen -->
  <xsl:variable name="text-replaced">
    <xsl:call-template name="string-replace-all">
      <xsl:with-param name="text" select="$text"/>
    </xsl:call-template>
    <!-- <xsl:copy-of select="$text"/> -->
  </xsl:variable>
  
  <!-- Anführungszeichen -->
  <xsl:call-template name="quotes">
    <xsl:with-param name="text" select="$text-replaced"/>
  </xsl:call-template>
</xsl:template>


<!-- Anführungszeichen -->
<xsl:template name="quotes">
  <xsl:param name="text"    select="''" />
  <xsl:param name="quote"   select="'&quot;'" />
  <xsl:param name="open"    select="'»'" />
  <xsl:param name="close"   select="'«'" />
  <xsl:param name="inquote" select="false()" />

  <xsl:choose>
    <!-- Anführungszeichen -->
    <xsl:when test="contains($text, $quote)">
      <xsl:value-of select="substring-before($text, $quote)" />
      <xsl:choose>
        <xsl:when test="$inquote">
          <xsl:value-of select="$close" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$open" />
        </xsl:otherwise>
      </xsl:choose>
      <xsl:call-template name="quotes">
        <xsl:with-param name="text"    select="substring-after($text, $quote)" />
        <xsl:with-param name="quote"   select="$quote" />
        <xsl:with-param name="open"    select="$open" />
        <xsl:with-param name="close"   select="$close" />
        <xsl:with-param name="inquote" select="not($inquote)" />
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$text" />
    </xsl:otherwise>
  </xsl:choose>  
</xsl:template>


</xsl:stylesheet>