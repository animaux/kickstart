<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:a="http://getsymphony.com/functions"
	xmlns:func="http://exslt.org/functions"
	xmlns:exslt="http://exslt.org/common"
	xmlns:dyn="http://exslt.org/dynamic"
	extension-element-prefixes="func dyn exslt a">

<xsl:output method="html" encoding="UTF-8" indent="no" />

<xsl:template match="/">

  <xsl:text disable-output-escaping="yes">&lt;</xsl:text>!DOCTYPE html<xsl:text disable-output-escaping="yes">&gt;</xsl:text>

  <html lang="de">
    <head>
      <meta charset="utf-8" />
      
      <title><xsl:value-of select="$website-name"/></title>
      <meta name="apple-mobile-web-app-title" content="{$website-name}" />
      <meta name="application-name" content="{$website-name}" /> 

      <meta name="description" content="" />
      <meta name="robots" content="index, follow" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
  
      <link href="{$workspace}/css/screen.css" rel="stylesheet" media="screen" type="text/css" />
      <!-- <link href="{$workspace}/css/print.css" rel="stylesheet" media="print" type="text/css" /> -->
      
      <link rel="icon" href="{$workspace}/img/favicon.ico" sizes="any"/>
			<link rel="icon" href="{$workspace}/img/favicon.svg" type="image/svg+xml"/>
			<link rel="apple-touch-icon" href="{$workspace}/img/favicon.png"/>
      
    </head>

    <body>

      <!-- Inhalt -->

    </body>
    
  </html>
</xsl:template>



<!-- Templates -->
<xsl:template name="temp">

</xsl:template>

</xsl:stylesheet>