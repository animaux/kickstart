<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	extension-element-prefixes="dyn exslt"
  xmlns:exslt="http://exslt.org/common"
  xmlns:dyn="http://exslt.org/dynamic">

	<!-- Version 2020-10-14 -->

  <!-- EVENT zum Überschreiben im Mutter-Template -->
  <xsl:variable name="default-event" select="'event-handle'" />
  <xsl:variable name="plus" select="'&#43;'" />



  <!-- Text-Inputs mit Fehlern und Post-Values -->
  <xsl:template name="input-text">
    <xsl:param name="title" />
    <xsl:param name="handle" />
    <xsl:param name="subhandle" />
    <xsl:param name="event" select="$default-event" />
    <xsl:param name="class" />
    <xsl:param name="id" />
    <xsl:param name="type" select="'text'" />
    <xsl:param name="required" select="'No'" />
    <xsl:param name="pattern" />
    <xsl:param name="value" />
    <label>
      <xsl:attribute name="class">
        <xsl:if test="(dyn:evaluate(concat('/data/events/', $event, '/', $handle, '/@message'))) or (dyn:evaluate(concat('/data/params/', $handle, '=&quot;', $plus, '&quot;')))">error </xsl:if>
        <xsl:value-of select="$handle"/>
        <xsl:if test="$subhandle!=''">
          <xsl:text> </xsl:text>
          <xsl:value-of select="$subhandle"/>
        </xsl:if>
        <xsl:if test="$class!=''">
          <xsl:text> </xsl:text>
          <xsl:value-of select="$class"/>
        </xsl:if>
      </xsl:attribute>
      <span>
				<xsl:value-of select="$title"/>
				<xsl:if test="$required = 'Yes'">
					<xsl:text>*</xsl:text>
				</xsl:if>
			</span>
      <input type="{$type}" placeholder="{$title}" title="{$title}">
        <xsl:attribute name="name">
          <xsl:text>fields</xsl:text>
          <xsl:text>[</xsl:text>
          <xsl:value-of select="$handle"/>
          <xsl:text>]</xsl:text>
          <xsl:if test="$subhandle!=''">
						<xsl:text>[</xsl:text>
						<xsl:value-of select="$subhandle"/>
						<xsl:text>]</xsl:text>
					</xsl:if>
        </xsl:attribute>
        <xsl:if test="$required = 'Yes'">
					<xsl:attribute name="required">required</xsl:attribute>
				</xsl:if>
				<xsl:if test="$id != ''">
					<xsl:attribute name="id"><xsl:value-of select="$id"/></xsl:attribute>
				</xsl:if>
        <xsl:if test="$pattern!=''">
          <xsl:attribute name="pattern">
            <xsl:value-of select="$pattern"/>
          </xsl:attribute>
        </xsl:if>
        <!-- Value -->
        <xsl:choose>
        	<xsl:when test="dyn:evaluate(concat('/data/events/', $event, '/post-values/', $handle))">
						<!-- Post-Values? (werden bevorzugt) -->
						<xsl:for-each select="dyn:evaluate(concat('/data/events/', $event, '/post-values/', $handle))">
							<xsl:attribute name="value">
								<xsl:choose>
									<xsl:when test="$subhandle != ''">
										<xsl:value-of select="dyn:evaluate($subhandle)"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="."/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:attribute>
						</xsl:for-each>
          </xsl:when>
          <xsl:otherwise>
          	<!-- Value-Parameter? -->
						<xsl:if test="$value != ''">
							<xsl:attribute name="value">
							  <xsl:value-of select="$value"/>
							</xsl:attribute>
						</xsl:if>
          </xsl:otherwise>
        </xsl:choose>
      </input>
    </label>
  </xsl:template>



  <!-- Textareas mit Fehlern und Post-Values -->
  <xsl:template name="textarea">
    <xsl:param name="title" />
    <xsl:param name="handle" />
    <xsl:param name="event" select="$default-event" />
    <xsl:param name="class" />
    <xsl:param name="required" select="'No'" />
    <xsl:param name="id" />
    <xsl:param name="value" />
    <label>
      <xsl:attribute name="class">
        <xsl:if test="dyn:evaluate(concat('/data/events/', $event, '/', $handle, '/@message'))">error </xsl:if>
        <xsl:value-of select="$handle"/>
        <xsl:if test="$class!=''">
          <xsl:text> </xsl:text>
          <xsl:value-of select="$class"/>
        </xsl:if>
      </xsl:attribute>
      <span>
				<xsl:value-of select="$title"/>
				<xsl:if test="$required = 'Yes'">
					<xsl:text>*</xsl:text>
				</xsl:if>
			</span>
      <textarea title="{$title}">
        <xsl:attribute name="name">
          <xsl:text>fields</xsl:text>
          <xsl:text>[</xsl:text>
          <xsl:value-of select="$handle"/>
          <xsl:text>]</xsl:text>
        </xsl:attribute>
        <xsl:if test="$id != ''">
					<xsl:attribute name="id"><xsl:value-of select="$id"/></xsl:attribute>
				</xsl:if>
				<xsl:if test="$required = 'Yes'">
					<xsl:attribute name="required">required</xsl:attribute>
				</xsl:if>
        <!-- Value -->
        <xsl:choose>
        	<xsl:when test="dyn:evaluate(concat('/data/events/', $event, '/post-values/', $handle))">
						<!-- Post-Values? (werden bevorzugt) -->
						<xsl:for-each select="dyn:evaluate(concat('/data/events/', $event, '/post-values/', $handle))">
							<xsl:value-of select="."/>
						</xsl:for-each>
          </xsl:when>
          <xsl:otherwise>
          	<!-- Value-Parameter? -->
						<xsl:if test="$value != ''">
							  <xsl:value-of select="$value"/>
						</xsl:if>
          </xsl:otherwise>
        </xsl:choose>
      </textarea>
    </label>
  </xsl:template>
  


  <!-- Checkboxen mit Fehlern und Post-Values -->
  <xsl:template name="input-checkbox">
    <xsl:param name="title" />
    <xsl:param name="handle" />
    <xsl:param name="value" select="'yes'" />
    <xsl:param name="multiple" select="'false'"/>
    <xsl:param name="event" select="$default-event" />
    <xsl:param name="required" select="'No'" />
    <xsl:param name="class" />
    <xsl:param name="checked" select="'false'" />
    <label>
      <xsl:attribute name="class">
        <xsl:value-of select="$handle"/>
        <xsl:text> checkbox</xsl:text>
        <xsl:if test="(dyn:evaluate(concat('/data/events/', $event, '/', $handle, '/@message'))) or (/data/params/art=0)"> error</xsl:if>
        <xsl:if test="$class!=''">
          <xsl:text> </xsl:text>
          <xsl:value-of select="$class"/>
        </xsl:if>
      </xsl:attribute>

      <input type="checkbox" value="{$value}" title="{$title}">
        <xsl:attribute name="name">
          <xsl:text>fields</xsl:text>
          <xsl:text>[</xsl:text>
          <xsl:value-of select="$handle"/>
          <xsl:text>]</xsl:text>
          <xsl:if test="$multiple = 'true'">
						<xsl:text>[]</xsl:text>
					</xsl:if>
        </xsl:attribute>
        <xsl:if test="$required = 'Yes'">
					<xsl:attribute name="required">required</xsl:attribute>
				</xsl:if>
        <!-- Post Values? (haben Priorität) -->
        <xsl:choose>
        	<xsl:when test="dyn:evaluate(concat('/data/events/', $event, '/post-values/', $handle, '/item=', $value)) or dyn:evaluate(concat('/data/events/', $event, '/post-values/', $handle, '=&quot;', $value, '&quot;'))">
						<xsl:attribute name="checked">checked</xsl:attribute>
					</xsl:when>
					<!-- Wenn keine Post-Values vorhanden, dann Wert aus Param -->
        	<xsl:otherwise>
            <xsl:if test="$checked = 'true'">
							<xsl:attribute name="checked">checked</xsl:attribute>
            </xsl:if>
          </xsl:otherwise>
        </xsl:choose>
      </input>
      <span>
				<xsl:copy-of select="$title"/>
				<xsl:if test="$required = 'Yes'">
					<xsl:text>*</xsl:text>
				</xsl:if>
			</span>
    </label>
  </xsl:template>
  
  
  
  <!-- Selects mit Fehlern und Post-Values -->
	<xsl:template name="select">
		<xsl:param name="title" />
		<xsl:param name="handle" />
		<xsl:param name="options" />
		<xsl:param name="multiple" select="'false'" />
		<xsl:param name="event" select="$default-event" />
		<xsl:param name="class" />
		<xsl:param name="value" />
		<xsl:param name="novalue" select="'Bitte auswählen …'"/>
		<xsl:param name="required" select="'No'" />
		<label>
			<xsl:attribute name="class">
			  <xsl:text>select </xsl:text>
				<xsl:if test="dyn:evaluate(concat('/data/events/', $event, '/', $handle, '/@message'))">error </xsl:if>
				<xsl:value-of select="$handle"/>
				<xsl:if test="$class!=''">
					<xsl:text> </xsl:text>
					<xsl:value-of select="$class"/>
				</xsl:if>
			</xsl:attribute>
			<span>
				<xsl:value-of select="$title"/>
				<xsl:if test="$required = 'Yes'">
					<xsl:text>*</xsl:text>
				</xsl:if>
			</span>
			<select title="{$title}">
			  <xsl:if test="$multiple = 'true'">
			    <xsl:attribute name="multiple" value="multiple"/>
			  </xsl:if>
			  <xsl:if test="$required = 'Yes'">
					<xsl:attribute name="required">required</xsl:attribute>
				</xsl:if>
				<xsl:attribute name="name">
					<xsl:text>fields</xsl:text>
					<xsl:text>[</xsl:text>
					<xsl:value-of select="$handle"/>
					<xsl:text>]</xsl:text>
					<xsl:if test="$multiple = 'true'">
						<xsl:text>[]</xsl:text>
					</xsl:if>
				</xsl:attribute>
				<option value="">
					<xsl:value-of select="$novalue"/>
				</option>
				<xsl:variable name="selection">
				  <xsl:choose>
				    <!-- Post Values gehen vor! -->
				  	<xsl:when test="dyn:evaluate(concat('/data/events/', $event, '/post-values/', $handle))">
				      <xsl:value-of select="dyn:evaluate(concat('/data/events/', $event, '/post-values/', $handle))"/>
				    </xsl:when>
				  	<xsl:otherwise>
				      <!-- Value-Parameter? -->
							<xsl:if test="$value != ''">
								<xsl:value-of select="$value"/>
							</xsl:if>
				    </xsl:otherwise>
				  </xsl:choose>
				  
				</xsl:variable>
				<xsl:for-each select="exslt:node-set($options)/option">
					<option value="{.}">
						<xsl:attribute name="value">
							<xsl:value-of select="@value"/>
						</xsl:attribute>
						<xsl:if test="$selection = @value">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>
						<xsl:value-of select="."/>
					</option>
				</xsl:for-each>
			</select>
		</label>  
	</xsl:template>
  
  

</xsl:stylesheet>
