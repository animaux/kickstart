<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:a="http://getsymphony.com/functions"
	xmlns:func="http://exslt.org/functions"
	xmlns:exslt="http://exslt.org/common"
	xmlns:dyn="http://exslt.org/dynamic"
	extension-element-prefixes="func dyn exslt a">
  
    
  <!--  
        
		Version 2.0
	
		Picks a translated string from a predefines set of strings in a variable. Uses the first available string if the desired langauge-code is not present in variable.

		USAGE 
		{a:lang('languagestringname')}
		<xsl:value-of select="a:lang('languagestringname')"/>
		<xsl:copy-of select="a:lang('languagestringname')"/>

		TRANSLATIONS STRING VARIABLE-EXAMPLE
		<xsl:variable name="langs">
			<languagestringname>
				<en>English text</en>
				<de>Deutscher Text</de>
			</languagestringname>
			<…>
		</xsl:variable>

		Expects the above variable `$langs`, as well as a param defining the current langauage, named `$lang` to be defined.
		
		You can also retrieve a string in a specific language by defining a second param. e. g.: {a:lang('languagestringname', 'en')}
	 	
		HTML inside the iso-code-nodes works too f. e. `<en>English <a href="#">link</a></en>`. For nodes inside language strings you need to use `xsl:copy-of` for retrieval.

	-->
	
	<func:function name="a:lang">
		<xsl:param name="string" select="''"/>
		<xsl:param name="lang" select="$lang"/>
		
		<func:result>
			<xsl:choose>
			  <!-- if string is not defined -->
				<xsl:when test="dyn:evaluate(concat('not(exslt:node-set($langs)/', $string, ')'))">
			    <xsl:text>undefined</xsl:text>
			  </xsl:when>
			
			  <!-- if requested langauage is NOT present … -->
				<xsl:when test="dyn:evaluate(concat('not(exslt:node-set($langs)/', $string, '/', $lang, ')'))">
				  <!-- … pick first language in variable -->
			    <xsl:for-each select="dyn:evaluate(concat('exslt:node-set($langs)/', $string, '/*[1]'))">
			      <!-- match text AND nodes inside the iso-code-node -->
			      <xsl:copy-of select="*|text()"/>
			    </xsl:for-each>
			  </xsl:when>

			  <!-- pick the requested language -->
				<xsl:otherwise>
			    <xsl:for-each select="dyn:evaluate(concat('exslt:node-set($langs)/', $string, '/', $lang))">
            <!-- match text AND nodes inside the iso-code-node -->
			      <xsl:copy-of select="*|text()"/>
			    </xsl:for-each>
			  </xsl:otherwise>
			</xsl:choose>
		</func:result>
		
	</func:function>

</xsl:stylesheet>