<?php

include __DIR__ . '/kirby/bootstrap.php';

$kirby = new Kirby([
    'roots' => [
        'media'    => __DIR__ . '/temp',
        'sessions' => __DIR__ . '/temp/sessions',
        'cache'    => __DIR__ . '/temp/cache'
    ],
		'urls' => [
        'media' => Url::home() . '/temp'
    ]
]);

echo $kirby->render();