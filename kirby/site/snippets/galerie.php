<!-- Galerie -->
<?php if ($galerie->isNotEmpty()) { ?>
	<ul class="galerie obj artikel">
		<?php foreach ($galerie as $figure): ?>
			<li>
				<figure>
					<img srcset="
						<?= $figure->srcset([
							'640w'  => ['height' => 360],
							'1280w' => ['height' => 720],
							'1920w' => ['height' => 1080]
						]) ?>"
					alt="<?= $figure->title() ?>"
					/>
					<figcaption><?= $figure->title() ?></figcaption>
				</figure>
			</li>
		<?php endforeach ?>
	</ul>
<?php } ?>