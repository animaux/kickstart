<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="utf-8" />
  
  <title><?= $slots->pagetitle() ?></title>
  
  <meta name="apple-mobile-web-app-title" content="<?= $site->title() ?>" />
  <meta name="application-name" content="<?= $site->title() ?>" /> 

  <!-- <meta name="description" content="" /> -->
  <meta name="robots" content="index, follow" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <?php /* force reload in debug mode */
    $forceRefresh = '';
    if (Config::get('debug') == true) {
      $forceRefresh = '?' . rand();
    }
		echo css(['assets/css/screen.css' . $forceRefresh, '@auto']);
	?>
  <!-- <link href="css/print.css" rel="stylesheet" media="print" type="text/css" /> -->
  
  <link rel="icon" href="<?= url('assets/img/favicon.ico') ?>" sizes="any"/>
	<link rel="icon" href="<?= url('assets/img/favicon.svg') ?>" type="image/svg+xml"/>
	<link rel="apple-touch-icon" href="<?= url('assets/img/favicon.png') ?>"/>
  
  <!-- JS -->
  <script>
    document.addEventListener('DOMContentLoaded', function() {
      
      /* externe Sites immer in neuem Fenster öffnen */
			function link_is_external(link_element) {
				return (link_element.host !== window.location.host);
			}
			var links = document.getElementsByTagName('a');
			for (var i = 0; i < links.length; i++) {
				if (link_is_external(links[i])) {
				  links[i].setAttribute('target','_blank');
				}
			}
      
    });
  </script>
  
  <!-- JS-Slot -->
  <?= $slots->js() ?>

  
</head>

  <body class="<?= $page->slug() ?>">
    <!-- Header -->
    <header></header>
    
    <!-- Content -->
    <main><div>
            
      <!-- Inhalt -->
			<?= $slots->inhalt() ?>
    </div></main>

  </body>

</html>