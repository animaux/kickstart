<h3 class="incognito">Download<?php e($Downloads->count() > 1, 's') ?></h3>
<ul class="downloads obj">
	<?php foreach ($Downloads as $download): ?>
			<li>
				<a href="<?= $download->url() ?>" title="Download: <?= $download->filename() ?>">
				  <?php $iconSVG = asset('assets/img/download.svg');
            echo $iconSVG->read(); ?>
          <span>
						<span class="titel">
							<?php
								if ($download->title()->isNotEmpty()) {
									echo $download->title();
								} else {
									echo $download->filename();
								}
							?>
						</span>
						<span class="ext"><?= $download->extension() ?></span>
						<span class="size"><?= $download->niceSize() ?></span>
					</span>
				</a>
			</li>
	<?php endforeach ?>
</ul>
