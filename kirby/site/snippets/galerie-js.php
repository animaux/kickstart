<script src="<?= url('assets/js/flickity.pkgd.min.js') ?>"></script>
<script src="<?= url('assets/js/fullscreen.js') ?>"></script>
<script>
	document.addEventListener('DOMContentLoaded', function() {
	
		<?php if (!empty($galerie)) { ?>
			let galerie = document.querySelector('ul.galerie');
			var flkty = new Flickity(galerie, {
				cellSelector: 'li',
				cellAlign: 'center',
				imagesLoaded: true,
				adaptiveHeight: true,
				contain: false,
				lazyLoad: true,
				fullscreen: true,
				on: {
					ready: function() {
						rpad();
					}
				}
			});
			// flkty.select(this.dataset.position);
			flkty.focus();
			flkty.on( 'fullscreenChange', function(isFullscreen) {
				window.dispatchEvent(new Event('resize'));
				flkty.resize();
			});
			// Abstand für die Pagination
			function rpad() {
				let Rpad = document.querySelector('.flickity-page-dots').offsetWidth;
				document.querySelectorAll('.flickity-enabled figcaption').forEach((figcaption) => {
					figcaption.style.paddingRight = "calc(" + Rpad + "px + 1.5em)";
				});
			}
		<?php } ?>
	}); 
</script>