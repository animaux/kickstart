<?php snippet('master', slots: true) ?>

  <?php slot('pagetitle') ?><?php
    // Seitentitel
    echo $site->title() . ' · ';
    if ($page->parent()) {
      echo $page->parent()->title();
      echo ' · ';
    }
    echo $page->title();
  ?><?php endslot() ?>




	<?php slot('js') ?>
	  <!-- JS Galerie -->
	  <?php if ($galerie->count() > 1): ?>
	    <!-- Galerie -->
			<?php snippet('galerie-js') ?>
    <?php endif ?>
  <?php endslot() ?>




  <?php slot('inhalt') ?>
    <article class="artikel pad default">
            
      <div class="text">
				<h2><?= $page->title() ?></h2>
				<?= $page->text()->typographer('block')->level(3) ?>
      </div>
      
      <!-- aside? -->
      <?php if ($galerie->isNotEmpty() || $page->files()->template('default')->isNotEmpty()) { ?>
        <aside>
					<!-- Galerie -->
					<?php snippet('galerie') ?>
					<!-- Downloads -->
					<?php snippet('downloads', ['Downloads' => $page->downloads()->toFiles()]) ?>
				</aside>
			<?php } ?>
      
    </article>
  <?php endslot() ?>
<?php endsnippet() ?>