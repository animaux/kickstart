<?php

return [
	'debug'  => false,
	'locale' => 'de_DE.utf-8',
	'home' => 'projekte',
  'date'  => [
    'handler' => 'intl'
	],
	'bnomei.recently-modified.format' => "dd. MMMM yyyy, HH:mm 'Uhr'",
  'hananils.typographer' => [
    'options' => [
      'word-length' => 128 // avoid usage by using a LONG value
    ]
  ],
  'panel' => [
    'css' => 'assets/css/panel.css',
    'js' => 'assets/js/panel.js',
    'favicon' => 'assets/img/p-favicon.ico',
  ],
  'markdown' => [
    'extra' => true
  ],
  'afbora.kirby-minify-html.enabled' => true,
    'afbora.kirby-minify-html.options' => [
			'doOptimizeViaHtmlDomParser'     => true,
			'doRemoveOmittedQuotes'          => false,
			'doRemoveOmittedHtmlTags'        => false
	],
	'routes' => [
			[
					'pattern' => 'projekte/(:any)/(:any)',
					'action' => function ($urlKat, $urlProj) {
							if ($page = page('projekte/' . $urlKat . '/' . $urlProj)) {
									return $page;
							} else {
								return page('projekte')->render([
									'urlKat' => $urlKat,
									'urlProj' => $urlProj
								]);
							}
					}
			],
			[
					'pattern' => 'projekte/(:any)',
					'action' => function ($urlKat) {
							if ($page = page('projekte/' . $urlKat)) {
									return $page;
							} else {
								return page('projekte')->render([
									'urlKat' => $urlKat
								]);
							}
					}
			]
	],
];